from Dictionnaire import Dictionnaire_principal

def separation(chaine_nom):
    """Permet de separer chaque lettre d'un mot""" 

    chaine_nom = chaine_nom.split()
    nouvelle_chaine = [lettre.upper() for lettre in chaine_nom] 
    return nouvelle_chaine

def premiere_lettre_prenom(nouvelle_chaine_nom):
    """On récupere la première lettre du prenom"""    
    premiere_lettre_prenom = nouvelle_chaine_nom[0]
    return premiere_lettre_prenom

def premiere_lettre_nom(nouvelle_chaine_nom):
    """On récupere la première lettre du nom"""    
    premiere_lettre_nom = nouvelle_chaine_nom
    return premiere_lettre_nom


def recuperation_premieres_lettres(nouvelle_chaine_nom):
    """On veut retourner une chaine de caractere contenant
    les premieres lettres du prenom et du nom"""

    new_premiere_lettre_prenom = premiere_lettre_prenom(nouvelle_chaine_nom)
    new_premiere_lettre_nom = premiere_lettre_nom(nouvelle_chaine_nom)

    premieres_lettres = new_premiere_lettre_prenom + new_premiere_lettre_nom
    return premieres_lettres

def comparaison_dictionnaire(premieres_lettres,univers):

    saisie_user = separation(premieres_lettres)
    deux_premiere_lettre = recuperation_premieres_lettres(saisie_user)

    prenom = deux_premiere_lettre[0]
    nom = deux_premiere_lettre[1] 

    prenom_dictionnaire = Dictionnaire_principal.reference_dictionnaire_prenom[univers]
    nom_dictionnaire =  Dictionnaire_principal.reference_dictionnaire_prenom[univers]
    nom_prenom_modifie = prenom_dictionnaire[prenom]
    nom_prenom_modifie = nom_dictionnaire[nom]

    return nom_prenom_modifie