import unittest 
from name_generator import *

class Testfonction(unittest.TestCase):

        def test_separation(self):
            self.assertEqual(separation('theo sauge'), ['T', 'H', 'E','O',  ' ', 'S', 'A', 'U', 'G', 'E'])
            self.assertEqual(separation('al ed'), ['A','L',  ' ', 'E', 'D'])
            self.assertEqual(separation('philippe h'), ['P','H', 'I', 'L', 'I', 'P', 'P', 'E', ' ', 'H'])
            self.assertEqual(separation('Hello'), ['H', 'E', 'L', 'L', 'O'])

        def test_selection_premiere_lettre(self):
            self.assertEqual(premiere_lettre_prenom('theo'), ['T'])
            self.assertEqual(premiere_lettre_prenom('al'), ['A'])
            self.assertEqual(premiere_lettre_nom('sauge'), ['S'])
            self.assertEqual(premiere_lettre_nom('ed'), ['E'])

            

