from tkinter import *
from tkinter.messagebox import *
from name_generator import *

fenetre = Tk()
fenetre.config(bg='light grey')
fenetre.title("Generateur de nom")

def button_SW_callback():
    retour.set(comparaison_dictionnaire(value.get(),'star_wars'))

 
def button_LOTR_callback():
    retour.set(comparaison_dictionnaire(value.get(),'lord_of_the_ring')) 

# affichage du titre du programme
typetitre = StringVar()
typetitre.set("Generateur de nom")
titre = Label(fenetre,textvariable=typetitre)
titre.grid(column=0, row=0,padx = 30,pady=10)
titre.config( width = 30, bg='grey')

# création des champs pour saisir le nom et le prénom
value = StringVar()
value.set("Entrez votre prenom")
input_prenom = Entry(fenetre, textvariable=value)
input_prenom.grid(column=0, row=1,padx = 30,pady=10 )
input_prenom.config( width = 30, bg='dark grey' ) 

value = StringVar()
value.set("Entrez votre nom")
input_nom = Entry(fenetre, textvariable=value)
input_nom.grid(column=0, row=2,padx = 30,pady=10 )
input_nom.config( width = 30, bg='dark grey' ) 

# création du champ resultat
retour = StringVar()
output_resultat = Entry(fenetre, textvariable = retour)
output_resultat.grid(column = 0, row=4,padx = 30,pady=10 ) 
output_resultat.config(width = 30,bg='dark grey' )

# création des boutons de selection d'univers

button_star_wars = Button(fenetre, text="Star Wars", command=button_SW_callback)
button_star_wars.grid(column=0, row=3, padx = 30,pady=10 )
button_star_wars.config(  bg = 'white', fg = 'black')

button_lord_of_the_ring = Button(fenetre, text="Lord Of The Ring", command=button_LOTR_callback)
button_lord_of_the_ring.grid(column=1, row=3, padx = 10,pady=10 )
button_lord_of_the_ring.config(  bg = 'white', fg = 'black')


fenetre.mainloop()