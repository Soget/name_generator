from Dictionnaire import Dictionnaire_SW
from Dictionnaire import Dictionnaire_LOTR

reference_dictionnaire_prenom = {
    'SW': Dictionnaire_SW.Dictionnaire_SW_prenom,
    'LOTR':Dictionnaire_LOTR.Dictionnaire_LOTR_prenom,
}

reference_dictionnaire_nom = {
    'SW': Dictionnaire_SW.Dictionnaire_SW_nom,
    'LOTR':Dictionnaire_LOTR.Dictionnaire_LOTR_nom,
}